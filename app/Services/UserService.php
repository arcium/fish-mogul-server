<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserService
{
    /**
     * Creates a user with specific settings
     *
     * @param array $settings
     */
    public function createUser(array $settings)
    {
        $user = new User;
        $user->email = $settings['email'];
        $user->password = bcrypt($settings['password']);
    }

    /**
     * Retrieves a user by email
     *
     * @param $email
     * @return User
     */
    public function retrieveUser($email)
    {
        return User::where('email', $email);
    }

    public function updateUser(User $user, array $settings)
    {
        //TODO: incremental not full, ie. POST not PUT
    }

    /**
     * Charges a user an amount
     *
     * @param User $user
     * @param float $amount
     * @return bool
     */
    public function chargeUser(User $user, $amount)
    {
        if($user->money < $amount)
        {
            return false;
        }

        DB::beginTransaction();

        try
        {
            $user->money -= $amount;
            $user->save();
            DB::commit();
        }
        catch(\Exception $e)
        {
            DB::rollBack();
            return false;
        }

        return true;
    }
}
