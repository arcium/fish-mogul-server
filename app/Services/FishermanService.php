<?php

namespace App\Services;

use App\Models\Fisherman;
use App\Models\HiredFisherman;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\DB;

class FishermanService
{
    /** @var UserService $userService */
    protected $userService;

    /**
     * @return UserService
     */
    public function getUserService()
    {
        return $this->userService;
    }

    /**
     * @param UserService $userService
     * @return FishermanService
     */
    public function setUserService($userService)
    {
        $this->userService = $userService;
        return $this;
    }

    public function __construct(UserService $userService)
    {
        $this->setUserService($userService);
    }

    public function retrieveCamelCaseFisherman(Fisherman $fisherman)
    {
        return [
            'firstName' => $fisherman->first_name,
            'lastName' => $fisherman->last_name,
            'speed' => $fisherman->speed,
            'accuracy' => $fisherman->accuracy,
            'tripLength' => $fisherman->trip_length,
            'boatCapacity' => $fisherman->boat_capacity,
            'price' => $fisherman->price,
        ];
    }

    public function seedFishermen()
    {
        Fisherman::truncate();

        $fishermenJson = '[
            {
                "id": 1,
                "firstName": "Kennedy",
                "lastName": "Blankenship",
                "speed": 9,
                "accuracy": 6,
                "tripLength": 5,
                "boatCapacity": 111,
                "price": 469
            },
            {
                "id": 2,
                "firstName": "Snider",
                "lastName": "Donovan",
                "speed": 5,
                "accuracy": 4,
                "tripLength": 6,
                "boatCapacity": 104,
                "price": 181
            },
            {
                "id": 3,
                "firstName": "Fry",
                "lastName": "Meyers",
                "speed": 8,
                "accuracy": 9,
                "tripLength": 3,
                "boatCapacity": 51,
                "price": 106
            },
            {
                "id": 4,
                "firstName": "Wade",
                "lastName": "Mueller",
                "speed": 5,
                "accuracy": 8,
                "tripLength": 9,
                "boatCapacity": 110,
                "price": 313
            },
            {
                "id": 5,
                "firstName": "Finley",
                "lastName": "Rutledge",
                "speed": 1,
                "accuracy": 10,
                "tripLength": 7,
                "boatCapacity": 160,
                "price": 300
            },
            {
                "id": 6,
                "firstName": "Frederick",
                "lastName": "Talley",
                "speed": 2,
                "accuracy": 2,
                "tripLength": 5,
                "boatCapacity": 150,
                "price": 424
            },
            {
                "id": 7,
                "firstName": "Frank",
                "lastName": "Delgado",
                "speed": 8,
                "accuracy": 1,
                "tripLength": 8,
                "boatCapacity": 58,
                "price": 101
            },
            {
                "id": 8,
                "firstName": "Butler",
                "lastName": "Alford",
                "speed": 2,
                "accuracy": 2,
                "tripLength": 10,
                "boatCapacity": 166,
                "price": 113
            },
            {
                "id": 9,
                "firstName": "Farrell",
                "lastName": "Pratt",
                "speed": 1,
                "accuracy": 4,
                "tripLength": 7,
                "boatCapacity": 135,
                "price": 188
            },
            {
                "id": 10,
                "firstName": "Rhodes",
                "lastName": "Ward",
                "speed": 8,
                "accuracy": 6,
                "tripLength": 7,
                "boatCapacity": 175,
                "price": 64
            },
            {
                "id": 11,
                "firstName": "Schneider",
                "lastName": "Abbott",
                "speed": 8,
                "accuracy": 5,
                "tripLength": 3,
                "boatCapacity": 154,
                "price": 144
            },
            {
                "id": 12,
                "firstName": "Myers",
                "lastName": "Brennan",
                "speed": 10,
                "accuracy": 7,
                "tripLength": 9,
                "boatCapacity": 131,
                "price": 442
            },
            {
                "id": 13,
                "firstName": "Salas",
                "lastName": "Clayton",
                "speed": 5,
                "accuracy": 4,
                "tripLength": 10,
                "boatCapacity": 98,
                "price": 367
            },
            {
                "id": 14,
                "firstName": "Sims",
                "lastName": "Hale",
                "speed": 7,
                "accuracy": 6,
                "tripLength": 4,
                "boatCapacity": 71,
                "price": 90
            },
            {
                "id": 15,
                "firstName": "Branch",
                "lastName": "Baird",
                "speed": 1,
                "accuracy": 5,
                "tripLength": 9,
                "boatCapacity": 125,
                "price": 497
            },
            {
                "id": 16,
                "firstName": "Sullivan",
                "lastName": "Glenn",
                "speed": 1,
                "accuracy": 5,
                "tripLength": 5,
                "boatCapacity": 122,
                "price": 265
            },
            {
                "id": 17,
                "firstName": "James",
                "lastName": "Cooley",
                "speed": 5,
                "accuracy": 3,
                "tripLength": 9,
                "boatCapacity": 96,
                "price": 73
            },
            {
                "id": 18,
                "firstName": "Torres",
                "lastName": "Calhoun",
                "speed": 7,
                "accuracy": 6,
                "tripLength": 1,
                "boatCapacity": 118,
                "price": 89
            },
            {
                "id": 19,
                "firstName": "Maynard",
                "lastName": "Stewart",
                "speed": 8,
                "accuracy": 2,
                "tripLength": 9,
                "boatCapacity": 195,
                "price": 42
            },
            {
                "id": 20,
                "firstName": "Sutton",
                "lastName": "Harrell",
                "speed": 7,
                "accuracy": 5,
                "tripLength": 10,
                "boatCapacity": 148,
                "price": 143
            }
        ]';

        $fishermen = json_decode($fishermenJson);

        foreach($fishermen as $fisherman)
        {
            $newFisherman = new Fisherman();
            $newFisherman->first_name = $fisherman->firstName;
            $newFisherman->last_name = $fisherman->lastName;
            $newFisherman->speed = $fisherman->speed;
            $newFisherman->accuracy = $fisherman->accuracy;
            $newFisherman->trip_length = $fisherman->tripLength;
            $newFisherman->boat_capacity = $fisherman->boatCapacity;
            $newFisherman->price = $fisherman->price;
            $newFisherman->save();
        }

        return Fisherman::all();
    }

    /**
     * Retrieves all available fishermen
     *
     * @return Fisherman[]
     */
    public function retrieveAllAvailable()
    {
        #TODO: remove inactive

        $fishermen = [];

        foreach(Fisherman::all() as $fisherman)
        {
            $fishermen[] = $this->retrieveCamelCaseFisherman($fisherman);
        }

        return $fishermen;
    }

    /**
     * Retrieves a single fisherman
     *
     * @param $fishermanId
     * @return array
     */
    public function retrieveFisherman($fishermanId)
    {
        #TODO: remove inactive
        $fisherman = Fisherman::findOrFail($fishermanId);

        return $this->retrieveCamelCaseFisherman($fisherman);
    }

    /**
     * Hires a fisherman
     *
     * @param User $user
     * @param Fisherman $fisherman
     * @return HiredFisherman
     */
    public function hireFisherman(User $user, Fisherman $fisherman)
    {
        #TODO:
        # wrap all this in a transaction
        # check if user can afford fisherman
        # check if fisherman is available

        try
        {
            $this->getUserService()->chargeUser($user, $fisherman->price);
        }
        catch(\Exception $e)
        {
            DB::rollBack();
        }

        $hiredFisherman = new HiredFisherman();
        $hiredFisherman->user_id = $user->id;
        $hiredFisherman->fisherman_id = $fisherman->id;
        $hiredFisherman->save();

        return $hiredFisherman;
    }
}
