<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fisherman extends Model
{
    protected $table = 'fishermen';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'speed',
        'accuracy',
        'trip_length',
        'boat_capacity',
        'price',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
