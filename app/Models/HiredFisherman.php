<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HiredFisherman extends Model
{
    protected $table = 'hired_fishermen';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'fisherman_id',
        'hire_time',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function fisherman()
    {
        return $this->hasOne('App\Models\Fisherman', 'id', 'fisherman_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
