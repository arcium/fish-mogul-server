<?php

namespace App\Providers;

use App\Services\FishermanService;
use Illuminate\Support\ServiceProvider;

class FishermanProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Services\UserService::class, function($app){
            return new FishermanService($app['UserService']);
        });
    }
}
