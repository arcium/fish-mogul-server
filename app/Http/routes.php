<?php

/*
 * User routes
 */

Route::post('api/user/authenticate', 'UserController@authenticate');
Route::get('api/user/retrieve', 'UserController@retrieve');

/*
 * Fisherman routes
 */

Route::get('api/fisherman/list', 'FishermanController@listFishermen');
Route::get('api/fisherman/retrieve', 'FishermanController@retrieveFisherman');
#Route::get('api/fisherman/seed', 'FishermanController@seed');
Route::post('api/fisherman/hire', 'FishermanController@hire');
