<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Requests;

use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        parent::__construct();

        $this->middleware('jwt.auth', ['except' => 'authenticate']);
        $this->middleware('jwt.refresh', ['except' => 'authenticate']);

        $this->setUserService($userService);
    }

    /**
     * @param UserService $userService
     * @return $this
     */
    protected function setUserService(UserService $userService)
    {
        $this->userService = $userService;

        return $this;
    }

    /**
     * @return UserService
     */
    protected function getUserService()
    {
        return $this->userService;
    }

    //
    // User actions
    //

    /**
     * Authenticates a user
     *
     * POST /api/user/authenticate
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try
        {
            // attempt to verify the credentials and create a token for the user
            if ( ! $token = JWTAuth::attempt($credentials))
            {
                return response()->json(['error' => 'invalid_credentials'], Response::HTTP_UNAUTHORIZED);
            }
        }
        catch (JWTException $e)
        {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    /**
     * Retrieves a single user
     *
     * GET /api/user/retrieve
     *
     * @return JsonResponse
     * @internal param Request $request
     */
    public function retrieve()
    {
        $user = $this->getUserService()->retrieveUser($this->getUser());

        return response()->json(['user' => $user]);
    }
}
