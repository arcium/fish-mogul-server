<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        #TODO: enable this
        #$this->middleware('jwt.auth');
        #$this->middleware('jwt.refresh');
    }

    /**
     * @return User
     */
    protected function getUser()
    {
        $user = Auth::user();

        if( ! $user)
        {
            throw new \Exception('Unable to load user', 500);
        }

        return $user;
    }

    /**
     * Returns a serialised result
     *
     * @param $data
     * @param int $code
     * @return array
     */
    protected function getSerialisedResult($data, $message = '', $code = 0)
    {
        return [
            'data' => $data,
            'message' => $message,
            'code' => $code,
        ];
    }
}
