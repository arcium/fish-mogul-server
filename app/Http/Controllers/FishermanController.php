<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\FishermanService;
use Illuminate\Http\Request;

use App\Http\Requests;

class FishermanController extends Controller
{
    private $fishermanService;

    public function __construct(FishermanService $fishermanService)
    {
        parent::__construct();

        $this->setFishermanService($fishermanService);
    }

    /**
     * @param FishermanService $fishermanService
     * @return $this
     */
    protected function setFishermanService(FishermanService $fishermanService)
    {
        $this->fishermanService = $fishermanService;

        return $this;
    }

    /**
     * @return FishermanService
     */
    protected function getFishermanService()
    {
        return $this->fishermanService;
    }

    //
    // Fisherman actions
    //

    /**
     * Retrieves all available fishermen
     *
     * GET /api/fisherman/list
     *
     * @param Request $request
     * @return array
     */
    public function listFishermen(Request $request)
    {
        $fishermen = $this->getFishermanService()->retrieveAllAvailable();

        return $this->getSerialisedResult(['fishermen' => $fishermen]);
    }

    /**
     * Retrieves a single fisherman
     *
     * GET /api/fisherman/retrieve
     *
     * @param Request $request
     * @return array
     */
    public function retrieveFisherman(Request $request)
    {
        $fisherman = $this->getFishermanService()->retrieveFisherman($request->get('fisherman_id'));

        return $this->getSerialisedResult(['fisherman' => $fisherman]);
    }

    /**
     * Hires a fisherman
     *
     * POST /api/fisherman/hire
     *
     * @param Request $request
     * @return array
     */
    public function hireFisherman(Request $request)
    {
        $fisherman = $this->getFishermanService()->retrieveFisherman($request->only('fisherman_id'));
        $hiredFisherman = $this->getFishermanService()->hireFisherman($this->getUser(), $fisherman);

        return $this->getSerialisedResult(['hired_fisherman' => $hiredFisherman]);
    }

    #TODO: remove
    public function seed()
    {
        return $this->getSerialisedResult(['fishermen' => $this->getFishermanService()->seedFishermen()]);
    }
}
